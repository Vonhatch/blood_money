﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    public Vector2 pos = new Vector2 (0f,0f);

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void setPos(int x, int y)
    {
        pos.x = x;
        pos.y = y;
    }

    public Vector2 getPos()
    {
        return pos;

    }
}
