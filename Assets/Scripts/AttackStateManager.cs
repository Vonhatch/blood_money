﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackState { idle, travelling, attacking, criticalWindow }

public class AttackStateManager: MonoBehaviour {

    public Animator animator;
    public AttackAbility ability;
    internal GameObject attacker;
    internal GameObject target;

    internal double totalElapsedTime;
    internal double travelTime;
    internal double distanceToTarget;

    internal double baseAttackTime;
    internal double criticalAttackTime;
    internal double totalAttackTime;
    internal double currentAttackElapsedTime;
    internal int numberOfAttacks;
    internal int completedAttacks;
    
    public AttackState currentState;

    public void init(Animator animator, AttackAbility attack)
    {
        this.animator = animator;
        this.currentState = AttackState.idle;
    }
    
    public void beginAttack(GameObject attacker, GameObject target)
    {
        if (currentState != AttackState.idle)
        {
            return;
        }

        this.attacker = attacker;
        this.target = target;

        
        totalElapsedTime = 0.0;
        currentAttackElapsedTime = 0.0;
        currentState = AttackState.travelling;

        double attackerSpeed = attacker.GetComponent<PlayerCharacter>().characterSpeed;
        distanceToTarget = Vector2.Distance(attacker.transform.position, target.transform.position);
        travelTime = distanceToTarget / attackerSpeed;
        baseAttackTime = ability.attackTime;
        criticalAttackTime = baseAttackTime - ability.criticalWindow;
        numberOfAttacks = ability.numberOfAttacks;
        totalAttackTime = baseAttackTime * numberOfAttacks;
    }

    void playAnimation(AttackState attackState)
    {
        switch (attackState)
        {
            case AttackState.attacking:
                animator.Play("slide");
                break;
            case AttackState.travelling:
                animator.Play("Running");
                break;
            case AttackState.idle:
                animator.Play("catIdle");
                break;
        }
    }

    void changeState(AttackState state)
    {
        if (currentState == state)
        {
            return;
        }

        currentState = state;
        playAnimation(state);
    }

	// Update is called once per frame
	void Update () {
        if (currentState == AttackState.idle)
        {
            return;
        }

        if (totalElapsedTime < travelTime)
        {
            float distanceToMove = (float)((Time.deltaTime / travelTime) * distanceToTarget);
            Vector2 targetPosition = target.transform.position;
            Vector2 attackerPosition = attacker.transform.position;
            attacker.transform.position = Vector2.MoveTowards(attackerPosition, targetPosition, distanceToMove);
        }
		else if (totalElapsedTime >= travelTime && completedAttacks < numberOfAttacks) {
            if (currentAttackElapsedTime >= baseAttackTime)
            {
                completedAttacks++;
                currentAttackElapsedTime = 0.0;
            }
            else if (currentAttackElapsedTime >= criticalAttackTime && currentAttackElapsedTime < baseAttackTime)
            {
                changeState(AttackState.criticalWindow);
            }
            else
            {
                changeState(AttackState.attacking);
            }
            currentAttackElapsedTime += Time.deltaTime;
        }
        else if (totalElapsedTime >= (travelTime + totalAttackTime))
        {
            float distanceToMove = (float)((Time.deltaTime / travelTime) * distanceToTarget);
            Vector2 targetPosition = target.transform.position;
            Vector2 attackerPosition = attacker.transform.position;
            attacker.transform.position = Vector2.MoveTowards(attackerPosition, targetPosition, -distanceToMove);
            changeState(AttackState.travelling);
        }
        else if (totalElapsedTime >= (travelTime + totalAttackTime + travelTime))
        {
            currentState = AttackState.idle;
        }

        totalElapsedTime += Time.deltaTime;
	}
}
