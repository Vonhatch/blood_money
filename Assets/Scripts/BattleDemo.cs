﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BattleDemo : MonoBehaviour
{
    public List<GameObject> actors = new List<GameObject>();
    public List<GameObject> targets = new List<GameObject>();

    //public GameObject enemy;
    //public GameObject target;

    public GameObject currentFighter;

    Vector3 enemyStartingLocation;
    bool actorFinished = true;

    //THIS MAY BE USED IF ENEMIES AND PLAYERS HAVE THE EXACT SAME SCRIPT ATTACHED
    bool playerTurn = false;
    bool playerDeciding = false;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        /*
        //this is a hack to make it easy to give characters commands without a ui, eventually to be replaced with a UI that just calls procedures here that do the same thing
        if (Input.GetAxis("Fire1") != 0 && currentFighter.GetComponent<PlayerCharacter>().player && actorFinished == false && playerDeciding == true)
        {
            playerDeciding = false;
            currentFighter.GetComponent<PlayerCharacter>().UseAttack1(currentFighter, actors[0]);
        }
        // this should prolly all be determined out of here? To make it best work when i combine it with a UI

        //MakeMove();
        */
        
    }

    private void MakeMove()
    {
        ///////must be at the end i think!!!!
        if (actorFinished == true)
        {
            //a very inefficient bubble sort to make sure everyone is ordered in terms of speed every time someone has acted
            for (int j = 0; j < actors.Count - 1; j++)
            {
                for (int i = 0; i < actors.Count - 1; i++)
                {
                    if (actors[i].GetComponent<PlayerCharacter>().modSpd < actors[i + 1].GetComponent<PlayerCharacter>().modSpd)
                    {
                        GameObject temp = actors[i];
                        actors[i] = actors[i + 1];
                        actors[i + 1] = temp;
                    }
                }
            }

            //decides who moves next
            for (int i = 0; i < actors.Count; i++)
            {
                //checks through list for the first actor in the list that hasn't moved this turn
                if (actors[i].GetComponent<PlayerCharacter>().HasAttackedThisTurn() != true && actorFinished == true && isDead(actors[i]) != true)
                {
                    actorFinished = false;
                    currentFighter = actors[i];
                    if (actors[i].GetComponent<PlayerCharacter>().player != true)
                    {
                        foreach (GameObject actor in actors)
                        {
                            if (actor.GetComponent<PlayerCharacter>().player == true && isDead(actor) != true)
                            {
                                //populates a list with potential targets
                                targets.Add(actor);
                            }
                        }
                        if (targets.Count > 0)
                        {
                            int targetNo = (int)Mathf.Floor(Random.Range(0f, ((float)targets.Count) - 0.001f));
                            actors[i].GetComponent<PlayerCharacter>().UseAttack1(actors[i], targets[targetNo]);
                        }
                        //this is just a temporary thing for if there's no targets
                        else
                        {
                            print("Game over, all ur doods are fuckin' dead");
                        }
                    }
                    else
                    {
                        playerDeciding = true;
                        foreach (GameObject actor in actors)
                        {
                            if (actor.GetComponent<PlayerCharacter>().player != true)
                            {
                                //populates a list with potential targets
                                targets.Add(actor);
                            }
                        }
                    }

                }
                //if the final actor in the list has moved then everyone has moved and start a next turn
                if (i == actors.Count - 1 && actorFinished == true)
                {
                    NewTurn();
                }
            }
            // currentActor++;
        }
        ///////must be at the end i think!!!!!!
    }

    private bool isDead(GameObject actor)
    {
        bool answer = false;
        foreach(GameObject state in actor.GetComponent<PlayerCharacter>().ailments)
        {
            if(state.gameObject.GetComponent<StatusEffect>().name == "dead")
            {
                answer = true;
            }
        }
        return answer;
    }

    //makes changes to the field to initiate the next turn
    private void NewTurn()
    {
        print("NEWWWWWWWWWWWWW TUUUUUUUUUUUUUUUUUURN");
        for (int i = 0; i < actors.Count; i++)
        {
            actors[i].GetComponent<PlayerCharacter>().NewTurn();
        }
    }

    //currently called by a character which is called by a move to notify the manager it can move on with play
    public void ActorHasFinished()
    {
        actorFinished = true;
        targets.Clear();
    }
}