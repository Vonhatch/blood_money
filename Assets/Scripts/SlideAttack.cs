﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType : int { physical = 0, fire = 1, cold = 2, electric = 3, typeless = 4 }

public class SlideAttack : MonoBehaviour {
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }


    //we only need a few attacks right? One for melee, shooting and technical? (should technical even be it's own thing?), and each of those a special animation/calculation if it's a crit!!

    /*
    damage calculators and indicators require these things
        1. the weapon type of both fighters
        2. the total atack value
        3. the total hit value
        4. the total evade value
        5. the total crit value
        6. the total crit evade value
        7. remaining health value
        8. max health value
        9. effective speed value
        10. skill trigger chance?
    */


    public void UseAttack1(GameObject attacker, GameObject attackee)
    {
        StartCoroutine(Attack2(attacker, attackee));
    }


    public void MeleeAttack(GameObject attacker, GameObject attackee)
    {
        StartCoroutine(Attack2(attacker, attackee));
    }

    public void RangeAttack(GameObject attacker, GameObject attackee)
    {
        StartCoroutine(Attack2(attacker, attackee));
    }

    //A TEST ATTACK FUNCTION, you may want to adjust this to accomodate any modifiers to how fast enemies attack to ensure damage and move frames happen at the same time
    IEnumerator Attack2(GameObject attacker, GameObject attackee)
    {
        float seconds = 1.0f;

        Vector3 attackerStartingLocation = attacker.transform.position;
        //used to ensure damage isn't dealt multiple times by the same move
        bool damageDealt = false;

        //used to dictate when the move will do damage
        float damageFrame = 0f;
        float endDamageFrame = 0f;

        float elapsedTime = 0;
        float newDuration = 0.0f;
        Vector3 startingPos = attacker.transform.position;
        Vector3 end = attackee.transform.position;

        end += new Vector3(Mathf.Clamp((attacker.transform.position.x - attackee.transform.position.x), -0.01f, 0.01f) * 100 * 6f, 0f, 0f);

        //for debugging purposes, we use this to change the colour of the enemy sprite to show when it's actively damaging
        SpriteRenderer enemyRenderer = attacker.GetComponent<SpriteRenderer>();

        //this tells the enemy to move to the starting location for the attack
        while (elapsedTime < seconds)
        {
            attacker.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        //this has the enemy wait a set amount of time before attacking
        elapsedTime = 0.0f;
        while (elapsedTime < 0.5f)
        {
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //this sets values used by the next attack
        elapsedTime = 0.0f;
        startingPos = attacker.transform.position;
        newDuration = 0.90f;
        end = attackee.transform.position - new Vector3(Mathf.Clamp((attacker.transform.position.x - attackee.transform.position.x), -0.01f, 0.01f) * 100 * 4f, 0f, 0f);
        damageFrame = 0.46f;
        endDamageFrame = 0.51f;

        attacker.GetComponent<Animator>().Play("slide"); //slide attack!
        while (elapsedTime < newDuration)
        {

            //debug stuff shows when the enemy can hurt the player
            if (elapsedTime > damageFrame)
            {
                enemyRenderer.color = new Color(0f, 0f, 0f);
            }
            if (elapsedTime > endDamageFrame)
            {
                enemyRenderer.color = new Color(255f, 255f, 255f);
            }

            //handles doing normal damage
            if (damageDealt == false && elapsedTime > damageFrame && elapsedTime < endDamageFrame && attacker.gameObject.GetComponent<PlayerCharacter>().IsSuperAttacking() != true)
            {

                attackee.gameObject.GetComponent<PlayerCharacter>().TakeDamage(10f, DamageType.fire);
                damageDealt = true;
            }
            //super damage
            else if (damageDealt == false && elapsedTime > damageFrame && elapsedTime < endDamageFrame && attacker.gameObject.GetComponent<PlayerCharacter>().IsSuperAttacking())
            {
                //critical attack now currently poisons....
                attackee.gameObject.GetComponent<PlayerCharacter>().TakeDamage(20f, 0, "poison", 1f);
                damageDealt = true;
            }

            attacker.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / newDuration));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //sets necessary values
        damageDealt = false;
        elapsedTime = 0.0f;

        //a wait after attacking
        while (elapsedTime < 0.5f)
        {
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //set values
        elapsedTime = 0.0f;
        startingPos = attacker.transform.position;
        newDuration = 1f;
        end = attackerStartingLocation;

        //returns to starting location
        while (elapsedTime < newDuration)
        {
            attacker.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / newDuration));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        attacker.gameObject.GetComponent<PlayerCharacter>().FinishedAttack();
    }
}
