﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {

    public List<GameObject> ailments = new List<GameObject>();

    //the actual pure stats of a character, unmodified by any status effects
    public int maxHealth = 100;
    public int health = 100;
    public int def = 50;
    public int str = 50;
    public int mag = 50;
    public int mdf = 50;
    public int spd = 50;
    public int dex = 50;
    public int lck = 50;

    public int movement = 5;

    //the stats actually used by the game for all calculations, these are affected by status ailments
    public float modMaxHealth = 100;
    public float modDef = 50;
    public float modStr = 50;
    public float modMag = 50;
    public float modMdf = 50;
    public float modSpd = 50;
    public float modDex = 50;
    public float modLck = 50;

    public int modMovement = 5;

    public float characterSpeed = 20;

    //variables related to dodging damage with right testing
    float defendframes = 0.2f;
    float defendTimer = 0.0f;
    float recoveryFrames = 0.5f;
    float recoveryTimer = 0.0f;
    


    /*an attack in this game must know: 
        1. what weapon it is with? (ie. melee, gun, technical (should i just pass an actual weapon ID in and the weapon tells the game what it is?)
        2. is it physical or technical?
        3. what type of damage is it?....... honestly maybe 2 and 3 can be combined, ie Mdf is just copied for all non physical types and then multipliers for special exceptions added.
     */


    public enum ResistanceType : int { physical = 0, fire = 1, cold = 2, electric = 3, typeless = 4 }

    //variables related to damage taken from each type
    float[] damageRes = new float[] {20f, 100f, 100f, 100f};

    //variables related to doing super damage with right timing
    float superDamageTimer = 0f;
    float superDamageRecoveryTimer = 0f;
    float superDamageWindow = 0.2f;
    float superDamageCooldown = 0.5f;

    bool attackedThisTurn = false;

    public bool player = false;


    // Use this for initialization
    void Start () {
        modMaxHealth = (float)maxHealth;
        modDef = def;
        modStr = str;
        modMag = mag;
        modMdf = mdf;
        modSpd = spd;
        modDex = dex;
        modLck = lck;

        float fireRes = getResistance(ResistanceType.fire);
        float fireRes2 = damageRes[0];

        setupInitialResistances(20, 30, 10, 40);

        setResistance(ResistanceType.fire, 2000);
}

    public float getResistance(ResistanceType type)
    {
        int index = (int)type;
        float resistance = damageRes[index];
        return resistance;
    }

    public ResistanceType getResistanceType(DamageType damageType)
    {
        switch (damageType) {
            case DamageType.fire:
                return ResistanceType.fire;
            case DamageType.cold:
                return ResistanceType.cold;
            case DamageType.physical:
                return ResistanceType.physical;
            case DamageType.electric:
                return ResistanceType.electric;
            case DamageType.typeless:
                return ResistanceType.typeless;
        }
        return ResistanceType.physical;
    }

    void setupInitialResistances(float fire, float ice, float armour, float poison)
    {
        ResistanceType[] resistanceTypes = (ResistanceType[])ResistanceType.GetValues(typeof(ResistanceType));

        //print(resistanceTypes.Length);
        foreach (ResistanceType resistanceType in resistanceTypes)
        {
            switch (resistanceType)
            {
                case ResistanceType.fire:
                    setResistance(resistanceType, fire);
                    break;
                case ResistanceType.physical:
                    setResistance(resistanceType, armour);
                    break;
                case ResistanceType.cold:
                    setResistance(resistanceType, ice);
                    break;
                case ResistanceType.electric:
                    setResistance(resistanceType, poison);
                    break;
            }
        }
    }

    void setResistance(ResistanceType type, float resistance)
    {
        int index = (int)type;
        damageRes[index] = resistance;
    }



    //this will be called in the attack function to determine the damage that will be dealt with the current weapon (but how do we get the damage type?! perhaps a small class to hold the necessary details?)
    int getDamageToDeal()
    {
        //this needs to return both a number and a damage type.....or i can get the number here and fetch the other values elsewhere? no this is dumb
        int damage = 0;
        return damage;
    }

    //starts the process for using the first attack (This will need further work)
    public void UseAttack1(GameObject attacker, GameObject attackee)
    {
        /*
        //////////////REMOVE THE BELOW LINE OF CODE AFTER TESTING!!!!!!!!!
        GameObject ailment = (GameObject)Instantiate(Resources.Load("Status"));
        ailment.GetComponent<StatusEffect>().name = "poop";
        ailment.GetComponent<StatusEffect>().damagePerTurn = 10f;
        ailment.GetComponent<StatusEffect>().magMod = 50f;

        ailments.Add(ailment);



        //////////////DELETE FROM ABOVE HERE!!!!!!!!!!!!
        */

        attackedThisTurn = true;
        gameObject.GetComponent<SlideAttack>().UseAttack1(attacker, attackee);
    }

    public void NewTurn()
    {
        attackedThisTurn = false;
    }

	// Update is called once per frame
	void Update () {

        //this starts a timer that dictates if a move reduced damage if it hits within the timer's timeframe, and also starts a longer recovery timer to prevent spamming
        defendTimer -= Time.deltaTime;
        recoveryTimer -= Time.deltaTime;
        if(defendTimer < -10f)
        {
            defendTimer = -1f;
        }
        if (recoveryTimer < -10f)
        {
            recoveryTimer = -1f;
        }
        if (Input.GetAxis("Fire1") != 0 && recoveryTimer <= 0f)
        {
            defendTimer = defendframes;
            recoveryTimer = recoveryFrames;
        }


        //this starts a timer that dictates if a move does extra damage if it hits within the timer's timeframe, and also starts a longer recovery timer to prevent spamming
        superDamageRecoveryTimer -= Time.deltaTime;
        if (superDamageRecoveryTimer < -10f)
        {
            superDamageRecoveryTimer = -1f;
        }
        superDamageTimer -= Time.deltaTime;
        if (superDamageTimer < -10f)
        {
            superDamageTimer = -1f;
        }
        if (Input.GetAxis("Fire2") != 0 && superDamageRecoveryTimer <= 0f && superDamageTimer <= 0f)
        {
            superDamageRecoveryTimer = superDamageCooldown;
            superDamageTimer = superDamageWindow;
        }
    }

    //checked by a move to see if it does optimal damage by succeeding an input
    public bool IsSuperAttacking()
    {
        if(superDamageTimer > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //checked by the battlemanager to see if the unit can move
    public bool HasAttackedThisTurn()
    {
        if (attackedThisTurn)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void FinishedAttack()
    {
        StartCoroutine(TurnEndCalcs());
    }

    IEnumerator TurnEndCalcs()
    {
        float elapsedTime = 0;
        float seconds = 0.4f;
        while (elapsedTime < seconds)
        {
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        foreach (GameObject ailment in ailments)
        {
            //checks if any ailments do damage over time and if so, does damage to the actor
            if (ailment.GetComponent<StatusEffect>().damagePerTurn > 0)
            {
                elapsedTime = 0;
                seconds = 1.0f;
                TakeDamage(ailment.GetComponent<StatusEffect>().damagePerTurn, ailment.GetComponent<StatusEffect>().damageType);
                //print("effect duration: " + ailments[0].GetComponent<StatusEffect>().duration.ToString());
                while (elapsedTime < seconds)
                {
                    elapsedTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
            }

            //reduces the duration of the ailment after each turn
            ailment.GetComponent<StatusEffect>().duration--;
        }

        //probably doesn't need to be here to be honest, just called every time a status is applied, frankly should just be handled by an apply status procedure eventually
        CalculateStatusChanges();

        elapsedTime = 0;
        seconds = 0.6f;
        while (elapsedTime < seconds)
        {
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        EndTurn();
    }

    private void EndTurn()
    {
        //removes all ailments with zero duration remaining
        ailments.RemoveAll(DurationIsZero);
        FindObjectOfType<BattleDemo>().ActorHasFinished();
    }

    private static bool DurationIsZero(GameObject status)
    {
        if (status.GetComponent<StatusEffect>().duration <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void GetStatusEffect(string name, float damage, int damageType)
    {
        GameObject ailment = (GameObject)Instantiate(Resources.Load("Status"));
        ailment.GetComponent<StatusEffect>().damagePerTurn = damage;
        ailment.GetComponent<StatusEffect>().damageType = (DamageType)damageType;

        ailment.GetComponent<StatusEffect>().name = name;

        if (ailment.GetComponent<StatusEffect>().canStack != true && ailments.Count>0)
        {

            for(int i = 0; i < ailments.Count; i++)
            {
                if (ailments[i].GetComponent<StatusEffect>().name == ailment.GetComponent<StatusEffect>().name)
                {
                    ailments[i] = ailment;
                }
            }
        }
        else
        {
            ailments.Add(ailment);
        }
        CalculateStatusChanges();
    }


    //to be called whenever the character is first affected by a status ailment and at the end of the character's turn
    public void CalculateStatusChanges()
    {
        //resets each modded stat to its base so it can be modified correctly again
        modDef = def;
        modStr = str;
        modMag = mag;
        modMdf = mdf;
        modSpd = spd;
        modDex = dex;
        modLck = lck;
        foreach (GameObject ailment in ailments)
        {
            //print(ailment.GetComponent<StatusEffect>().magMod.ToString());
            modMaxHealth *= ailment.GetComponent<StatusEffect>().maxHealthMod/100f;
            modStr *= ailment.GetComponent<StatusEffect>().strMod / 100f;
            modDef *= ailment.GetComponent<StatusEffect>().defMod / 100f;
            modDex *= ailment.GetComponent<StatusEffect>().dexMod / 100f;
            modLck *= ailment.GetComponent<StatusEffect>().lckMod / 100f;
            modMdf *= ailment.GetComponent<StatusEffect>().mdfMod / 100f;
            modMag *= ailment.GetComponent<StatusEffect>().magMod / 100f;
            modSpd *= ailment.GetComponent<StatusEffect>().spdMod / 100f;
        }
    }



    //the damage calculation formulae, this takes the damage number and other information from an attack and calculates its results using the character's stats
    //THIS MUST BE MODIFIED SOONER RATHER THAN LATER TO ALSO REQUIRE A DAMAGE TYPE (PROBABLY USING AN INT TO REPRESENT EACH TYPE OF DAMAGE)
    public void TakeDamage(float damage, DamageType damageType)
    {
        if (defendTimer > 0)
        {
            print("dodged damage");
        }
        else
        {
            int index = (int)getResistanceType(damageType);
            print("Took " + ((int)(damage * damageRes[index] / 100f)).ToString() + " damage!");
            print(damageRes.ToString());
            this.gameObject.GetComponent<Animator>().Play("Hurt");

            /* develop an actual calculation here and make sure the calculations are put in every other take damage function everywhere */


            health -= (int)(damage*damageRes[index] /100f);
        }


        if (health <= 0)
        {
            Die();
        }
    }

    public void TakeDamage(float damage, int damageType, string statusEffect, float statusEffectChance)
    {
        if (defendTimer > 0)
        {
            print("dodged damage");
        }
        else
        {
            print("Took " + ((int)(damage * damageRes[damageType] / 100f)).ToString() + " damage!");
            this.gameObject.GetComponent<Animator>().Play("Hurt");

            health -= (int)(damage * damageRes[damageType]/100f);


            /* develop an actual calculation here and make sure the calculations are put in every other take damage function everywhere */
            if( Random.value < statusEffectChance)
            {
                GetStatusEffect(statusEffect, damage, damageType);
            }
            
        }

        if(health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        health = 0;
        this.gameObject.GetComponent<Animator>().Play("die");
        ailments.Clear();
        GetStatusEffect("dead", 0, 0);
    }

}
