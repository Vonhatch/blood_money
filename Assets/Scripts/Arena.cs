﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour {

    private GameObject[,] tileList = new GameObject[11, 10];
    public GameObject testTile;
    public GameObject testTile2;
    public GameObject highlightTile;

    public GameObject mouseIndicator;

    public GameObject testCharacter;

    private Vector3 currentMousePos;
    private Vector3 mousePos;


    private Vector2 prevMouseCoord;
    private Vector2 curMouseCoord;


    public List<GameObject> actors = new List<GameObject>();

    public GameObject[] Arrows;

    public List<GameObject> arrowList = new List<GameObject>();

    private bool startTracking = false;

    // Use this for initialization
    void Start () {
        mousePos = new Vector3(0f, 0f, 0f);

        prevMouseCoord = new Vector2(0f, 0f);
        curMouseCoord = new Vector2(0f, 0f);

        for (int i =0; i < tileList.GetLength(0); i++)
        {
            for(int j = 0; j < tileList.GetLength(1); j++)
            {
                GameObject currentTile = new GameObject();
                currentTile = testTile;
                //Vector3 pos = new Vector3;
                Instantiate(currentTile, new Vector3 ((i*2f),j*(2f) -1f ,4f), this.transform.rotation);
                currentTile = tileList[i, j];


            }
        }

        /*
        //Destroy(tileList[0, 0]);
        GameObject currentTile2 = new GameObject();
        currentTile2 = highlightTile;
        Instantiate(currentTile2, new Vector3(0f * 2f, 0f * (-2f) + 9f, 1f), this.transform.rotation);
        currentTile2 = tileList[0, 0];
        */

        GameObject dood = new GameObject();
        dood = testCharacter;
        Instantiate(dood, new Vector3((0f * 2f), 0f * (-2f) + 9f, 0f), this.transform.rotation);
        //dood = tileList[0, 0]; //do i want to save them in an array? I feel like no

        actors.Add(dood);
        //print(tileList.GetLength(0) + " " + tileList.GetLength(1));


    }

    // Update is called once per frame
    void Update () {

        currentMousePos = Input.mousePosition;
        currentMousePos = Camera.main.ScreenToWorldPoint(currentMousePos);
        

        //FOR FUTURE REFERENCE, exchange 2 below for whatever value multiple you want the results to be one of
        mouseIndicator.transform.position = new Vector3(2*Mathf.Round(currentMousePos.x/2),-1f + (2 * Mathf.Round((currentMousePos.y + 1f )/ 2)) , 0f);


        if (Input.GetMouseButtonDown(0))
        {
            print("true");
            startTracking = true;
        }

        if (Input.GetMouseButtonDown(1))
        {
            foreach(GameObject dieArrow in arrowList)
            {
                Destroy(dieArrow);
            }
            arrowList.Clear();
        }

        if (startTracking)
        {
            currentMousePos = Input.mousePosition;
            currentMousePos = Camera.main.ScreenToWorldPoint(currentMousePos);
            
            curMouseCoord.x = Mathf.Clamp( (2*Mathf.Round(currentMousePos.x / 2)), 0f, 2f * (tileList.GetLength(0) - 1) );
            curMouseCoord.y = Mathf.Clamp((2 * Mathf.Round((currentMousePos.y +1f)/ 2)), 0f, 2f * (tileList.GetLength(1) - 1) );

            if (curMouseCoord.x != prevMouseCoord.x || curMouseCoord.y != prevMouseCoord.y)
            {
                //GameObject theArrow = Arrows[0];

                //GameObject theArrow = Arrows[10];

                GameObject theArrow = Instantiate(Arrows[10], new Vector3(curMouseCoord.x, curMouseCoord.y - 1f, 1f), this.transform.rotation);            
                theArrow.GetComponent<Arrow>().setPos((int)curMouseCoord.x, (int)curMouseCoord.y);

                //Instantiate(theArrow , new Vector3(curMouseCoord.x, curMouseCoord.y - 1f, 1f), this.transform.rotation);

                //theArrow.GetComponent<Arrow>().setPos((int)curMouseCoord.x, (int)curMouseCoord.y);

                //Instantiate(theArrow, new Vector3(curMouseCoord.x, curMouseCoord.y - 1f, 1f), this.transform.rotation);

                //int offendingNumber = 9999;



                foreach (GameObject obj in arrowList)
                {
                    if(theArrow.GetComponent<Arrow>().getPos().Equals( obj.GetComponent<Arrow>().getPos() ))
                    {
                        int objIndex = arrowList.IndexOf(obj);
                        int count = arrowList.Count;
                        for (int i = objIndex; i < count; i++)
                        {
                            Destroy(arrowList[objIndex]);
                            arrowList.RemoveAt(objIndex);
                        }
                        break;
                    }
                }

                //important
                arrowList.Add(theArrow);


                
                if(arrowList.Count > 1)
                {
                    //right
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x) {
                        GameObject replacement = Instantiate(Arrows[2], new Vector3(arrowList[arrowList.Count - 1].transform.position.x, arrowList[arrowList.Count - 1].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 1]);
                        arrowList[arrowList.Count - 1] = replacement;
                    }
                    //right
                    else if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x)
                    {
                        GameObject replacement = Instantiate(Arrows[6], new Vector3(arrowList[arrowList.Count - 1].transform.position.x, arrowList[arrowList.Count - 1].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 1]);
                        arrowList[arrowList.Count - 1] = replacement;
                    }
                    //up
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[0], new Vector3(arrowList[arrowList.Count - 1].transform.position.x, arrowList[arrowList.Count - 1].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 1]);
                        arrowList[arrowList.Count - 1] = replacement;
                    }
                    //down
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[4], new Vector3(arrowList[arrowList.Count - 1].transform.position.x, arrowList[arrowList.Count - 1].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 1]);
                        arrowList[arrowList.Count - 1] = replacement;
                    }
                }
                if (arrowList.Count > 2)
                {
                    //this is so fucking ugly
                    //downright

                    //|| (arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y)
                    if (  ( arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().y) 
                            || (arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y) )
                    {
                        GameObject replacement = Instantiate(Arrows[3], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //upright
                    else if ((arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().y)
                        || ( arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)  )
                    {
                        GameObject replacement = Instantiate(Arrows[1], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //upleft
                    else if ((arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().x)
                        || (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().y))
                    {
                        GameObject replacement = Instantiate(Arrows[7], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //downleft
                    else if ((arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().x)
                        || (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x && arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 3].GetComponent<Arrow>().getPos().y))
                    {
                        GameObject replacement = Instantiate(Arrows[5], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    else if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x)
                    {
                        GameObject replacement = Instantiate(Arrows[9], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //left arrow
                    else if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x)
                    {
                        GameObject replacement = Instantiate(Arrows[9], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //up
                    else if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[8], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //down
                    else if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[8], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
               
                    if(arrowList.Count == 3)
                    {
                        GameObject replacement = Instantiate(Arrows[10], new Vector3(arrowList[0].transform.position.x, arrowList[0].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[0].GetComponent<Arrow>().getPos().x, (int)arrowList[0].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[0]);
                        arrowList[0] = replacement;
                    }


                }
                /*else if (arrowList.Count > 1)
                {
                    if(arrowList[arrowList.Count - 1 ].GetComponent<Arrow>().getPos().x > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x)
                    {
                        GameObject replacement = Instantiate(Arrows[2], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //left arrow
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().x < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x)
                    {
                        GameObject replacement = Instantiate(Arrows[6], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //up
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y > arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[0], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                    //down
                    if (arrowList[arrowList.Count - 1].GetComponent<Arrow>().getPos().y < arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y)
                    {
                        GameObject replacement = Instantiate(Arrows[4], new Vector3(arrowList[arrowList.Count - 2].transform.position.x, arrowList[arrowList.Count - 2].transform.position.y, 1f), this.transform.rotation); ;
                        replacement.GetComponent<Arrow>().setPos((int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().x, (int)arrowList[arrowList.Count - 2].GetComponent<Arrow>().getPos().y);

                        Destroy(arrowList[arrowList.Count - 2]);
                        arrowList[arrowList.Count - 2] = replacement;
                    }
                }*/

                prevMouseCoord.x = curMouseCoord.x;
                prevMouseCoord.y = curMouseCoord.y;
            }


            //print(curMouseCoord.x.ToString() + " " + curMouseCoord.y.ToString());
            //-10 10
            //10 -8
        }

        /*here's what i think, add an arrow class with a coordinate position on it, 
        when you move to a new tile check the list for any arrows that were already on that tile,
        if there was a tile there then delete that tile and any tiles after it,
        get the previous tile's position and, relative to the new tile's position, update its graphic
        */

        //list items also need a direction

        //to remove all items after, get that tile's location in the list? can lists do that? list.IndexOf (item)

        //(-10f, 9f,4f) starting pos

    }
}
