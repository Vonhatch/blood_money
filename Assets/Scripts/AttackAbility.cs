﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum DamageType { fire, ice, poison, physical }

public class AttackAbility : BaseAbility {

    public double baseDamageMultiplier;
    public double criticalDamageMultiplier;
    public DamageType damageType;
    public double passiveAilmentChance;
    public double criticalAilmentChance;
    public double attackTime;
    public int numberOfAttacks;
    public double criticalWindow;

    public AttackStateManager stateManager;

    public void init(AttackStateManager stateManager, string abilityName, TargetType targetType, int maxTargets, double baseDamageMultiplier, double criticalDamageMultiplier, DamageType damageType, double passiveAilmentChance, double criticalAilmentChance, double criticalWindow)
    {
        this.stateManager = stateManager;
        this.abilityName = abilityName;
        this.targetType = targetType;
        this.maxTargets = maxTargets;
        this.baseDamageMultiplier = baseDamageMultiplier;
        this.criticalDamageMultiplier = criticalDamageMultiplier;
        this.damageType = damageType;
        this.passiveAilmentChance = passiveAilmentChance;
        this.criticalAilmentChance = criticalAilmentChance;
        this.criticalWindow = criticalWindow;
    }
     
    public double calculateDamage(double baseDamage, bool critical)
    {
        double damageDealt = baseDamage * baseDamageMultiplier;
        if (critical)
        {
            damageDealt *= criticalDamageMultiplier;
        }
        return damageDealt;
    }
}
