﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTile : MonoBehaviour {

    //the image file

    //should these be multipliers or straight additions?
    public float evadeValue = 1f;
    public float hitValue = 1f;

    public int defendValue = 0;
    public int attackValue = 0;

    public int regenValue = 1;

    public float moveValue = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
